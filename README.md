[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4687109.svg)](https://doi.org/10.5281/zenodo.4687109)

# Space cooling needs density map of buildings in EU28 + Switzerland, Norway and Iceland for the year 2015


## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```
## Documentation
This dataset shows the space cooling needs density map of buildings in EU28 on hectare (ha) level.  

The cooling demand is derived based on the average cooling needs for buildings on the NUTS0 level per floor area and is adoped based on the ratio between national cooling degree days and calculated local cooling degree days.
**It is important to keep in mind that the cooling needs significantly deviate from the observed electricity consumption for air conditioning.
Besides the included uncertainties caused by the approach, the deviation is driven by the efficiency of AC systems (with a typical COP in the range of 2-4) and the fact, that for most regions the share of building floor area, which is fully air-conditioned is well below 1.**

For detailed explanations and a graphical illustration of the dataset please see the [Hotmaps WP2 report](https://www.hotmaps-project.eu/wp-content/uploads/2018/03/D2.3-Hotmaps_for-upload_revised-final_.pdf) section 2.1.3.


### Limitations of the dataset
For the use and the estimation of the reliability of the data it is important to keep in mind that the data maps build on a statistical approach and do not take site specific or local conditions into account.


### References
Müller, A., Hummel, M., Kranzl, L., Fallahnejad, M., Büchele, R., 2019. Open Source Data for Gross Floor Area and Heat Demand Density on the Hectare Level for EU 28. Energies 12, 4789. https://doi.org/10.3390/en12244789


## How to cite
Müller, A., Hummel, M., Kranzl, L., Fallahnejad, M., Büchele, R., 2019. Open Source Data for Gross Floor Area and Heat Demand Density on the Hectare Level for EU 28. Energies 12, 4789. https://doi.org/10.3390/en12244789


## Authors
Andreas Mueller <sup>*</sup>

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/), Institute of Energy Systems and Electrical Drives, Gusshausstrasse 27-29/370, 1040 Wien


## License
Copyright © 2016-2019: Andreas Mueller
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.

